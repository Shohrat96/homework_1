function filterBy(arr,exclud) {
    let newArr=[];
    arr.forEach(item=>{
        if ((typeof item)!==exclud){
            newArr.push(item);
        }
    });
    return newArr;
}

console.log(filterBy(['Gogi', 'Mogi', 123, 'Tetto'], 'number'));