let input1=prompt('please insert first number: ');
while(isNaN(input1) || input1==="" || isNaN(parseInt(input1))){ //check against:1.if isNan 2.not left empty 3. if the input is not only "probel"
    input1=prompt('please insert first number in a correct form: ',input1);
}
let num1=parseInt(input1);
let input2=prompt('please second second number: ');
while(isNaN(input2) || input2==="" || isNaN(parseInt(input2))){ //check against:1.if isNan 2.not left empty 3. if the input is not only "probel"
    input2=prompt('please insert second number in a correct form: ',input2);
}
let num2=parseInt(input2);
let operation=prompt('enter the operation: possible values: +, -, *, / :');
let operationList=['+','-','*','/'];
while (!operationList.includes(operation)){ //check if operation is possible
    operation=prompt('enter the operation: possible values: +, -, *, / :',operation);
}
//declaring the function
function performer(num1,num2,operation){
    let result;
    switch (operation) {
        case "+":
            result= num1+num2;
            break;
        case "-":
            result= num1-num2;
            break;
        case "*":
            result= num1*num2;
            break;
        case "/":
            result= num1/num2;
            break;
        default :
            alert ('something went wrong')
    }
    return result;
}
console.log(` the result of operation: ${performer(num1,num2,operation)}`);
