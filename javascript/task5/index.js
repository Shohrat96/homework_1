//getting tabs and tab-titles as an array
let tabs=document.querySelectorAll('.tabs-item');
let tabsArray=Array.from(tabs);
let tabTitles=document.querySelectorAll('.tabs-title');
let tabTitlesArray=Array.from(tabTitles);

//defining toggler function to execute display:on/off for tabs
function tabToggler(item){
    let className=item.textContent.toLowerCase();
    if (!(item.classList.contains('active'))){
        if (document.querySelector(`.tabs-item-${className}`)){
            document.querySelector(`.tabs-item-${className}`).style.display='none';
        }
    }else if (item.classList.contains('active')){
        if (document.querySelector(`.tabs-item-${className}`)){
            document.querySelector(`.tabs-item-${className}`).style.display='block';
        }
    }
}
//event handler for click on tab-titles
tabTitlesArray.forEach(item=>{
    tabToggler(item);
    item.addEventListener('click', (e)=>{
        let curActive=document.querySelector('.active');
        curActive.classList.remove('active');
        console.log(curActive);
        tabToggler(curActive);
        item.classList.toggle('active');
        tabToggler(item);
    })
});