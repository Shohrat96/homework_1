import React, { Component } from 'react';
import ProductCard from './ProductCard'
import './style/productList.scss';
import ModalWindow from './popUp/ModalWindow';
import Button from './popUp/Button';

class ProductList extends Component {
  constructor(props){
    super(props);
    this.state={
      productList:[],
      isLoaded:false,
      modalShowAdd:false,
      modalShowDel:false,
      likedCards:[],
      purchasedCards:[],
    }
  }
  addToFavorite = (item)=>{
    let likedCardsNew=[];

    if (localStorage.likedItems){
      likedCardsNew=JSON.parse(localStorage.likedItems);
    } else{
      likedCardsNew=this.state.likedCards;
    }
      this.state.productList.filter(product=>{
        if (product.id.toString()===item.id){
          likedCardsNew.push(product);
        }
      })
      this.setState({
          likedCards:likedCardsNew
      })
    localStorage.setItem('likedItems',JSON.stringify(likedCardsNew));
    return localStorage;
  }
  removeFromFav = (item)=>{
    console.log(item);
      let id=item.id;
      console.log(item.id);
    let likedCards=this.state.likedCards;
    console.log(likedCards);
    let afterFilter=likedCards.filter(item=>item.id!=id);
    this.setState({
        likedCards:afterFilter
    })
    localStorage.setItem('likedItems',JSON.stringify(afterFilter));
    return localStorage;
  }
  cardID=null;
  addToCardHandler=(cardID)=>{
    let isAdded=null;
    if (localStorage.purchasedCards){
      isAdded=JSON.parse(localStorage.purchasedCards).find(card=>card.id==cardID);
    }
    if (isAdded){
      let filteredFromAdded=JSON.stringify(JSON.parse(localStorage.purchasedCards).filter(item=>item.id!=isAdded.id));
      localStorage.setItem('purchasedCards',filteredFromAdded);
    } else{
      let curAddedToCard=[]
      if (localStorage.purchasedCards){
        curAddedToCard=JSON.parse(localStorage.purchasedCards);
      }    
      let card=this.state.productList.find(item=>{
        return item.id==cardID;
      })
      curAddedToCard.push(card);
      this.setState({
        purchasedCards:curAddedToCard
      })
      localStorage.setItem('purchasedCards',JSON.stringify(curAddedToCard));
    }
      this.setState({
        modalShowAdd:false,
        modalShowDel:false
      })
  }
  toggleModal=(ev)=>{
          let isAdded=false;
          if (ev.target.classList.contains('add-to-fav')){
            this.addToCardHandler(this.cardID);
          }
          if (ev.target.classList.contains('add-to-card')){
            this.cardID=ev.currentTarget.id;
            console.log(ev.target);
            console.log(this.cardID);
            if (localStorage.purchasedCards){
              isAdded=JSON.parse(localStorage.purchasedCards).find(card=>card.id==this.cardID);
            }
            if (isAdded){
              this.setState({
                modalShowDel:true,
            })
            } else {
              this.setState({
                modalShowAdd:true,
            })
            }
          }
          else if (ev.target.classList.contains('modal-window-main-wrapper') || ev.target.classList.contains('close-button') || ev.target.textContent==='CANCEL'){
            this.setState({
                modalShowAdd:false,
                modalShowDel:false
            })
          }
          this.lastAddToCard=ev.currentTarget.id;

};

  componentDidMount(){
    fetch("products.json")
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            productList: result,
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
      if (localStorage['likedItems']){
        let extractFromLocalStor=JSON.parse(localStorage['likedItems']);
        this.setState({likedCards:extractFromLocalStor});
      }

  }
  render() {
    let localCurrState=[]
    if (localStorage.purchasedCards){
      localCurrState=JSON.parse(localStorage.purchasedCards);
    }
    
    return (
      <div className={'productList'}>
        <ProductCard purchasedItems={localCurrState} likedItems={this.state.likedCards} addToFavorite={this.addToFavorite} removeFromFav={this.removeFromFav}  onClick={this.toggleModal} itemList={this.state.productList}/>  
        {
            this.state.modalShowAdd ===true ? <ModalWindow onClick={this.toggleModal} background={'#e74c3c'} disableModal={this.disableModal}
            header={'This is to confirm adding to card'} closeButton={true} text={'Are you sure you want to add it to card?'} actions={[<Button classNames={'button add-to-fav'} textContent={'OK'} background={'#b3382c'} />, <Button textContent={'CANCEL'} classNames={'button'} background={'#b3382c'}/>]}
            /> : 
            this.state.modalShowDel===true ? <ModalWindow onClick={this.toggleModal} background={'#e74c3c'} disableModal={this.disableModal}
            header={'This is to confirm deleting from card'} closeButton={true} text={'Are you sure you want to delete it from card?'} actions={[<Button classNames={'button add-to-fav'} textContent={'OK'} background={'#b3382c'} />, <Button textContent={'CANCEL'} classNames={'button'} background={'#b3382c'}/>]}
            /> : null
        }
      </div>
        
    );
  }
}

export default ProductList;

