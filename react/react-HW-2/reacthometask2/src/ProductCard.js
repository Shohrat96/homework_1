import React, { Component } from 'react';
import './style/productCard.scss';

class ProductCard extends Component {
    constructor(props){
        super(props);    
    }
    handleCardClick=(e)=>{
        let el=e.target;
        if (el.classList.contains('add-to-card')){

            this.props.onClick(e);
        }


        if (el.classList.contains('item-header-favourite')){

            if (el.src.indexOf("favorite-unpress.png")!=-1){
                el.src="./img/favorite-press.png"
                this.props.addToFavorite(e.currentTarget);
            }else {
                el.src="./img/favorite-unpress.png"
                this.props.removeFromFav(e.currentTarget);
            }
        }

    }
    render() {
        let likedOnes=[];
        let purchasedItems=this.props.purchasedItems;
        console.log(purchasedItems);
        return (
            this.props.itemList.map((item)=>{
                this.props.likedItems.filter(prod=>{if(prod.id==item.id){
                    likedOnes.push(prod.id);
                }   
            });
                return (
            <div onClick={this.handleCardClick} id={item.id} className={'productItem'}>
                <div className={'productItem-image'}>
                    <img src={item.Path} alt={item.Path}></img>
                </div>
                <div className={'productItem-descript'}>
                    <span className={'productItem-descript-header'}>
                        <h1 className={'item-header-title'}>{item.Name}</h1> 
                        <button className={'addToCardBtn'} >
                            {
                                likedOnes.indexOf(item.id)!==-1 ? <img src='./img/favorite-press.png' className={'item-header-favourite'}></img> :
                                <img src='./img/favorite-unpress.png' className={'item-header-favourite'}></img>
                            }
                        </button>
                    </span>
                    <p className={'productItem-descript-text'}>
                        Lorem ipsum dolor sit amet, conadipiscing elit, sed diam nonu.
                    </p>
                    <div className={'productItem-descript-footer'}>
                        <span className={'item-price'}>{item.Price}</span>
                        {
                            purchasedItems.find(card=>card.id==item.id) ? <button className={'add-to-card'}>REMOVE FROM CARD</button>
                            : <button className={'add-to-card'}>ADD TO CARD</button>
                        }
                        
                    </div>
                
                </div>
            </div>
                )
            })
            
        );
    }
}

export default ProductCard;