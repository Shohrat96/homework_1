import React, { Component } from 'react';
import ProductList from './ProductList'
import AddedToCard from './pages/AddedToCard';
import AddedToFav from './pages/AddedToFav';
import { Route, Link } from 'react-router-dom';
import './popUp/App.css';
class App extends Component {
  
  render() {
    return (
      <div className='app-wrapper'>
        <div className='app-nav'>
          <Link to="/">Home</Link>
          <Link to="/addedToCard">Added To Card</Link>
          <Link to="/addedToFavorite">Added To Favorite</Link>
        </div>

        <Route exact path="/" component={ProductList}/>
        <Route path="/addedToCard" component={AddedToCard}/>
        <Route path="/addedToFavorite" component={AddedToFav}/>

        
      </div>
        
    );
  }
}

export default App;

