import React from 'react';

const AddedToFav = () => {
    return (
        <div>
            {
                localStorage.likedItems ? 
                JSON.parse(localStorage.likedItems).length>0 ?
                <h1>LIST OF FAVORITE ITEMS ISN'T EMPTY BUT UNFORTUNATELY COULDN'T HAVE TIME TO RENDER ITEMS SORRY !( </h1>
                : <h1>LIST OF FAVORITE ITEMS IS EMPTY</h1> : null
            }
        </div>
    );
};

export default AddedToFav;