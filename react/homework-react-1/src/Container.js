import React, {Component} from 'react';
import Button from './Button'
import propTypes from 'prop-types';

import './style/index.css'
import ModalWindow from './ModalWindow'
class Container extends Component {
    state={
        modalShowFirst:null,
        modalShowSec:null,
    };
    showModalFirst=(buttonID)=>{
        this.setState({
            modalShowFirst:true
        })
    };
    showModalSecond=(buttonID)=>{
        this.setState({
            modalShowSec:true
        })
    };
    disableModal=(e)=>{
        if ((e.target.classList.contains('modalWindowWrapper')) || e.target.classList.contains('close-button') ||
            e.target.classList.contains('second-modal-close')
        ){
            this.setState({
                modalShowFirst:null,
                modalShowSec:null
            })
        }
    };
    render() {
        return (
            <div className="container">

                <Button textContent={"OpenFirstModal"} background={'#af82d6'} onclickFunction={this.showModalFirst} />
                <Button textContent={"OpenSeconModal"} background={'#48f204'} onclickFunction={this.showModalSecond} />

                {
                    this.state.modalShowFirst===true ? <ModalWindow background={'#e74c3c'} disableModal={this.disableModal}
                                                         header={'Do you want to delete this file?'} closeButton={true} text={'Once you delete this file, it won’t be possible to undo this action. \n' +
                    'Are you sure you want to delete it?'} actions={[<Button textContent={'OK'} background={'#b3382c'}/>, <Button textContent={'CANCEL'} background={'#b3382c'}/>]}


                    /> : null
                }
                {
                    this.state.modalShowSec===true ? <ModalWindow  background={'#2C2C21'} disableModal={this.disableModal}
                                                                   header={'Please read the conditions below: '} closeButton={true} modalNum={'second'} text={'The Intellectual Property disclosure will inform users that ' +
                    'the contents, logo and other visual media you created is your property and is protected by copyright laws.'} actions={[<Button textContent={'Agree'} background={'black'}/>, <Button textContent={"Disagree"} background={'black'} />]}


                    /> : null
                }

            </div>
        );
    }


};

export default Container;