import React, {Component} from 'react';
import './style/index.css'
import propTypes from 'prop-types';
class Button extends Component{
    render() {
        return (
            <button className="button" style={{backgroundColor:this.props.background}} onClick={this.props.onclickFunction}>{this.props.textContent}</button>
        );
    }
}

Button.propTypes={
    background : propTypes.string,
    onclickFunction : propTypes.func,
    textContent:propTypes.string
};

export default Button;